package pos.machine;

import java.util.ArrayList;
import java.util.List;

public class Receipt {
    private final List<ReceiptItem> receiptItems;
    private int totalPrice;


    public Receipt(List<ReceiptItem> receiptitems, int totalPrice) {
        this.receiptItems = receiptitems;
        this.totalPrice = totalPrice;
    }


    public List<ReceiptItem> getReceiptitems() {
        return receiptItems;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

}
