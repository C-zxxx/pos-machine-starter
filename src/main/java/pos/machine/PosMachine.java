package pos.machine;

import java.util.ArrayList;
import java.util.List;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = decodeToItems(barcodes);
        Receipt receipt = calculateCost(receiptItems);

        String finalReceipt = renderReceipt(receipt);
        return finalReceipt;
    }

    public List<Item> loadAllItems() {
        List<Item> items = ItemsLoader.loadAllItems();
        return items;
    }

    public List<ReceiptItem> decodeToItems(List<String> barcodes) {

        List<Item> items = loadAllItems();

        List<ReceiptItem> receiptItems = new ArrayList<ReceiptItem>();

//        for (int i = 0; i < barcodes.size(); i++) {
//            String barcode = barcodes.get(i);
//
//            Item item = items.stream().filter(good -> barcode.equals(good.getBarcode())).findAny().orElse(null);
//
//            ReceiptItem receiptItem = new ReceiptItem(item.getName(), 1, item.getPrice(), 0);
//            receiptItems.add(receiptItem);
//        }
//        replace as follow
        barcodes.stream().forEach(barcode -> {
            Item item = items.stream().filter(good -> barcode.equals(good.getBarcode())).findAny().orElse(null);
            ReceiptItem receiptItem = new ReceiptItem(item.getName(), 1, item.getPrice(), 0);
            receiptItems.add(receiptItem);
        });

        return receiptItems;
    }

    public List<ReceiptItem> calculateItemsCost(List<ReceiptItem> receiptItems) {
        int receiptItemsCount = 0;
        List<ReceiptItem> receiptItemsNew = new ArrayList<ReceiptItem>();
//        for (int i = 0; i < receiptItems.size(); i++) {
//            ReceiptItem receiptItem = receiptItems.get(i);
//            int j;
//            for (j = 0; j < receiptItemsNew.size(); j++) {
//                if (receiptItem.getName() == receiptItemsNew.get(j).getName()) {
//                    receiptItem.setQuantity(receiptItemsNew.get(j).getQuantity() + 1);
//                    receiptItem.setSubTotal(receiptItem.getUnitPrice() * receiptItem.getQuantity());
//                    receiptItemsNew.set(j, receiptItem);
//                    break;
//                }
//            }
//            if (j == receiptItemsNew.size()) {
//                receiptItemsNew.add(j, receiptItem);
//            }
//        }
//      replace
        receiptItems.stream().forEach(receiptItem -> {
            int j;
            for (j = 0; j < receiptItemsNew.size(); j++) {
                if (receiptItem.getName() == receiptItemsNew.get(j).getName()) {
                    receiptItem.setQuantity(receiptItemsNew.get(j).getQuantity() + 1);
                    receiptItem.setSubTotal(receiptItem.getUnitPrice() * receiptItem.getQuantity());
                    receiptItemsNew.set(j, receiptItem);
                    break;
                }
            }
            if (j == receiptItemsNew.size()) {
                receiptItemsNew.add(j, receiptItem);
            }
        });

        return receiptItemsNew;
    }

    public int calculateTotalPrice(List<ReceiptItem> receiptItems) {
        int totalPrice = 0;
        for (int i = 0; i < receiptItems.size(); i++) {
            totalPrice += receiptItems.get(i).getSubTotal();
        }

//        totalPrice = receiptItems.stream().reduce(0, (pre, b) -> pre.getSubTotal() + b.getSubTotal());

        return totalPrice;
    }

    public Receipt calculateCost(List<ReceiptItem> receiptItems) {
        receiptItems = calculateItemsCost(receiptItems);
        int totalPrice = calculateTotalPrice(receiptItems);
        Receipt receipt = new Receipt(receiptItems, totalPrice);
        return receipt;
    }

    public String generateItemsReceipt(Receipt receipt) {
        String itemsReceipt = "";
        List<ReceiptItem> receiptItems = receipt.getReceiptitems();
//        for (int i = 0; i < receiptItems.size(); i++) {
//            ReceiptItem receiptItem = receiptItems.get(i);
//            itemsReceipt += String.format("Name: %s, Quantity: %d, Unit price: %d (yuan), Subtotal: %d (yuan)\n",
//                            receiptItem.getName(), receiptItem.getQuantity(), receiptItem.getUnitPrice(), receiptItem.getSubTotal());
//        }
//      replace
        receiptItems.stream().forEach(receiptItem -> {
            itemsReceipt += String.format("Name: %s, Quantity: %d, Unit price: %d (yuan), Subtotal: %d (yuan)\n",
                    receiptItem.getName(), receiptItem.getQuantity(), receiptItem.getUnitPrice(), receiptItem.getSubTotal());
        });

        return itemsReceipt;
    }

    public String generateReceipt(String itemsReceipt, int totalPrice) {
        String receipt = "";
        receipt += "***<store earning no money>Receipt***\n"
                + itemsReceipt + "----------------------\n"
                + "Total: " + totalPrice + " (yuan)\n"
                + "**********************";
        return receipt;
    }

    public String renderReceipt(Receipt receipt) {
        return generateReceipt(generateItemsReceipt(receipt), receipt.getTotalPrice());
    }

}
