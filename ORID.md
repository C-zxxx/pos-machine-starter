
**O:**
  >* 1.Tasking：Simple task, Division of labor & Exclusive testing, verifiable  
  >* 2.Context Map:  
    what is in the context map: box with name - function name, arrow pointing to box - input, arrow pointing away from the box - output  
    Naming in context map:  
    ①Reveal intention  
    ②Avoid incomprehensible abbreviations  
    ③Name with  business expression  
    ④Use consistent format  
  >* 3.Some tips in using Git:  
    ①Submit frequently in small steps  
    ②Do not submit incomplete changes  
    ③Test change before submit  
    ④Detail commit message: type: message  
    type can as this: feat(feature)/fix/refactor/test/chore/docs e.g.
  >* 4.How to convert concept maps into specific functions and code.
- - -
**R:** Benefiting greatly, but a bit confused.
- - -
**I:** Learning has entered the code stage, providing me with more practical opportunities to test my learning achievements and consolidating my newly learned knowledge, but there is a bit of new knowledge.
- - -
**D:** In the following code projects, try to use the concept map method for tasks as much as possible.
